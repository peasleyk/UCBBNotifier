import sys
import time
import os
import argparse
import getpass
import logging
import json
import requests

import yaml
from selenium import webdriver
from selenium.common import exceptions


class Push ():
  def __init__(self,key):
    self.key = key
    self.logger = logging.getLogger('notify')

  def send_push(self, usd, title):
    """
      Sends a pushbullet message to the specified user (key)
      Takes the message, and the title of the message
    """
    if not self.key:
      self.logger.warn("No Pb key, not sending push")
      return

    data = {
        "type" : "note",
        "title" : usd,
        "body" : title
      }
    headers = {
        "Content-Type" : "application/json",
        "Access-Token" : self.key
      }

    data = json.dumps(data)
    url = 'https://api.pushbullet.com/v2/pushes'


    req = requests.request("POST", url, data=data, headers=headers)
    if not req.ok:
      self.logger.error("Could not send message. Is pushbullet API key correct?")
      return

    self.logger.info('Sent notification')
    self.logger.debug(req)

class UCCB():

  def __init__(self,username, password,refresh = None,key=None):
    self.logger = logging.getLogger('notify')
    self.refresh = refresh
    self.username = username
    self.password = password
    self.pb = Push(key)
    self.url = "https://eacct-bearcat-sp.blackboard.com/eAccounts/AnonymousHome.aspx"

    try:
      self.driver = webdriver.PhantomJS()
      self.driver = self.navigate(username, password)
    except Exception as e:
      logger.error("Can't start phantomjs driver!")
      logger.error(e)

  def navigate(self, p_username, p_password):
    """
      Takes in UC username, Password and returns a Seleniumd driver with the current page being
      the one with out current balance printed

      This will navigate the website to get to the current balance page, and return it
      Multiple try catches as we need to see if a id/link changed on their end
    """
    self.logger.info("navigating to signin...")

    self.driver.get(self.url)
    try:
      signinButton = self.driver.find_element_by_id('MainContent_SignInButton')
      signinButton.click()
    except exceptions.NoSuchElementException:
      self.logger.error("Can't find sign in button! Is the url or ID right?")
      quit(self.driver)

    try:
      username = self.driver.find_element_by_id("username")
      password = self.driver.find_element_by_id("password")
      username.send_keys(p_username)
      password.send_keys(p_password)
    except exceptions.NoSuchElementException:
      self.logger.error("Can't find sign in buttons! Is url right?")
      quit(self.driver)

    try:
      self.driver.find_element_by_name("_eventId_proceed").click()
    except exceptions.NoSuchElementException:
      self.logger.error('Could not find log in button!')
      quit(self.driver)

    return self.driver


  def fail(self,failCount,failcheck):
    """
      If we fail to find the balence 15 times in a row, then send us a notification
      that we should probably restart the script
    """
    if failCount == 5:
      self.pb.send_push("notify.py has failed to refresh 5 times, quitting", "Alert!")
      try:
        quit(self.driver)
      except AttributeError:
        self.logger.error("Exiting")
        sys.exit()
    if failcheck == 1:
      self.logger.debug("failCount added %s", failCount)
      return failCount+1
    if failcheck == 0:
      self.logger.debug("failCount reset %s",failCount)
      failCount = 0
      return failCount

  def keep_alive_mode(self,failCount=0):
    """
      This will keep running indefinitely, or when maintenance starts
      Balance will be checked every X minutes defined in config.yml
      Any changes are pushed to the device
      The inputed driver must be on the main Bearcat card page (from navigate())
    """

    current = 0
    self.logger.info("Starting keep alive mode")

    while(True):
      usd = self.return_usd()

      if not usd:
        #If for some reason we can't find the total or it times out, re start the server
        #need to find this exception if/when it happens
        failCount = self.fail(failCount,1)
        self.logger.info("Rerunning script")
        self.driver = self.navigate(self.username,self.password)
        self.keep_alive_mode(failCount)
      if usd != current:
        self.pb.send_push(usd,'Amount remaining')
        current = usd
      else:
        self.logger.info("Amount the same")

      # Wait the number of minutes defined in our config
      self.logger.info('Waiting...')
      time.sleep(self.refresh*60)

      try:
        # This just refreshed the webpage, so the new total can be found
        self.driver.refresh()
        self.logger.info("Refreshing driver...")
        failCount = self.fail(failCount,0)
      except Exception as error:
        self.logger.error(error)
        self.logger.info(("Driver could not be refreshed, restarting driver"))
        # The page could not be refreshed, did it time out?
        self.driver.quit()
        failCount = self.fail(failCount,1)
        self.driver = self.navigate(self.username,self.password)
        self.keep_alive_mode(failCount)

  def return_usd(self):
    """
      Finds the USD total of the inputed page and returns it
    """
    self.logger.info("Finding balance...")

    try:
      # Needs to be a wild card as the id changes per session
      total = self.driver.find_element_by_xpath(("//*[contains(@id,'MainContent_StoredValueAccountBalanceLabel')]"))
    except exceptions.NoSuchElementException:
      self.logger.error("Could not find balance. Did you enter your credentials correctly?")
      return
    except AttributeError:
      self.logger.error("Could not access driver object. Is the url right?")
      return None

    # Sometimes total will be a selenium webElement
    try:
      total = total.text
    except AttributeError:
      return None

    return total


def main(argv):
  try:
    config = yaml.safe_load(open("config.yml"))
  except Exception:
    print("Please fill out config.yml")
    sys.exit()

  logger = logging.getLogger('notify')
  logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s",level=config["logging"]["level"],datefmt='%m/%d/%Y %I:%M:%S')
  if os.environ.get('debug') is not None:
    logger.setLevel(logging.DEBUG)

  logger.debug("Starting debug...")

  parser = argparse.ArgumentParser()
  parser.add_argument("-s", "--single", help="Notifies you once of your bearcat card balance", action='store_true')
  parser.add_argument("-c", "--continuous", help="Runs the script continuously, sending you a notification when your balance changes.", action='store_true')
  parser.add_argument("-p", "--password", help="Asks you for your username/password, bypassing having to have stored credentials", action='store_true')
  parser.parse_args()
  results = vars(parser.parse_args())

  if results['continuous'] or results['single']:
    if results['password']:
      username = input('username: ')
      password = getpass.getpass()
    else:
      try:
        username = config["credentials"]["username"]
        password = config["credentials"]["password"]
      except IOError:
        logger.error("config.yml does not exist!")
        quit()
      if not username:
        logger.error("username is empty in config file! Exiting")
        quit()
      if not password:
        logger.error("Password is empty in config file! Exiting")
        quit()

    if results['single']:
      notifier = UCCB(username,password)
      usd = notifier.return_usd()
      logger.info("Balance: %s" %usd)
      quit(notifier)

    refresh = config['refresh_time']
    key = config['key']
    notifier = UCCB(username,password,refresh,key)
    notifier.keep_alive_mode()

  else:
    parser.print_help()

def quit(notifier=None):
  """
    Internal method to quit the program to kill phantomjs processes
  """

  # Quit our web driver if it's present
  if notifier:
    notifier.driver.quit()

  os.system('pkill -f phantomjs')
  logger.info('Quitting')
  sys.exit(0)

if __name__ == "__main__":
  try:
    main(sys.argv[1:])
  except KeyboardInterrupt:
    quit()



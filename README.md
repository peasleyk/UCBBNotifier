UCCB Notifier
==========
What?
-----
This script is a hacked together way to get notifications when you spend money on your BearCat card.
This grew out of frustration of having to log on to the website as I regularly forget my balance even though UC cash registers will tell you.
This uses selenium with phantomjs to create a headless browser that navigates to the Bearcat card website, and monitors the balance. Changes are checked every 5 minutes by default.

Pushbullet is used to send current balance details

Features
-----------
These are the current script options:
```
usage: notify.py [-h] [-s] [-c] [-p]
optional arguments:
  -h, --help        show this help message and exit
  -s, --single      Notifies you once of your bearcat card balance
  -c, --continuous  Runs the script continuously, sending you a notification
                    when your balance changes.
  -p, --password    Asks you for your username/password, bypassing having to
                    have stored credentials
```
Example:

```
python notify.py -s
INFO: navigating to signin...
INFO: Finding balance...
INFO: Balance: xx.xx USD
```

Setup
---------
Install the requirements with pipenv (requirements.txt is used for docker)

```
pipenv install
pipenv shell
```

If using pushbullet for continuous notifications, get a key from your pushbullet accounts settings.
Edit config.yml to include your key,  UC username and password if not using -p flag, and the minutes the script should wait before checking a balance change.
You also need phantomjs from a javascript package manager(npm) or from source,
npm for example:
```
npm -g install phantomjs-prebuilt
```
Please set your npm global folder outside of a root directory before this - https://docs.npmjs.com/getting-started/fixing-npm-permissions.


Docker
-------
A docker file is provided if you want to throw this script in a service on some host, or just want to manage it easily with docker.
It requires a filled out config.yml with your UC username and password, along with your pushbullet key. It will run 'notify.py -c' by default.

To build it 

```
docker build -t <name> .
```

To run
```
docker run <name>
```

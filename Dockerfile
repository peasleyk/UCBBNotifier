FROM alpine:3.8

MAINTAINER Kyle Peasley

COPY UCCBNotifier.py /
COPY Pipfile /
COPY Pipfile.lock /
COPY config.yml /

RUN apk add python3  \
            npm \
            bzip2 \
            fontconfig \
            freetype \
            which \
 && npm install phantomjs-prebuilt -g --unsafe-perm \
 && pip3 install --upgrade pip \
                           pipenv \
 && pipenv install --system

#ENTRYPOINT ["python3"]
CMD ["python3","UCCBNotifier.py","-c"]
